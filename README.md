# Schedule



## [What is a container?](https://github.com/titansmc/singularity-training-2019/raw/master/1.-singularity-training-what-are-containers.odp)

- **9.30am**

**Slides - https://github.com/titansmc/singularity-training-2019/raw/master/1.-singularity-training-what-are-containers.odp**

* What is Singularity?


## [How do I create my own containers?](https://slides.com/mbhall88/making-containers)

- **10.00am**

**Slides - https://slides.com/mbhall88/making-containers**

- Writing "recipe"/definition files
- Recipe development using a sandbox and the shell command
- Remote builds


## Playing with containers build

- **11.30am**

## Lunch break

- **12.30am**
 
## Playing with containers build

- **01.15pm**

## How do I run a container on the EMBL cluster? Running services as instances (RStudio. MongoDB...) 

- **2.00pm**

* MongoDB example - https://git.embl.de/grp-bio-it/singularity-service-example

* Cluster wiki - https://wiki.embl.de/cluster/Singularity#RStudio_in_the_cluster

* How to access data inside the containers**

## [How do I access containers written by other people and/or share my own?](https://slides.com/mbhall88/remote-container-systems)

- **03.00pm**

**Slides - https://slides.com/mbhall88/remote-container-systems**

* Singularity Hub
* Sylabs Cloud
* Docker Hub
* Quay.io and Biocontainers

---

## [How do I use containers with workflow systems?](https://slides.com/mbhall88/singularity-and-workflow-management-systems)

- **04.00pm**

**Slides - https://slides.com/mbhall88/singularity-and-workflow-management-systems**
- Snakemake
- Nextflow

---

## [Troubleshooting, common mistakes, best practices](https://git.embl.de/moscardo/singularity-training-2019/raw/master/slides/2.-singularity-training-Installation-and-troubleshooting.odp)

- **04.20pm**

**Slides - https://github.com/titansmc/singularity-training-2019/blob/master/2.-singularity-training-Installation-and-troubleshooting.odp**

